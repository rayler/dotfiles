# dotfiles
These are my dotfiles for my system running Linux on a Macbook Pro, These should be mostly Linux/nix-independent but just in case they aren't

# Screenshots
## My i3 status bar
(https://imgur.com/7xjXivZ)

## My workflow
(https://imgur.com/HH8yHvl)
(Images weren't rendered correctly)

# Some Info here
+ Pushing Changes from Debian 10 Buster(for now)
+ My dotfiles are managed by by YADM(https://yadm.io/) {Really Great}

# Software I use
+ Ganoo+Lincuck Debian(95% of the time spent here) and Gentoo(Still love Arch and Void). Also sometimes macOS
+ Simple Terminal for my Terminal
+ i3-gaps(compiled for Debian using https://github.com/maestrogerardo/i3-gaps-deb). Sometimes also use dwm and Gnome
+ Zathura for pdf reader
+ Chromium(for now)
+ Emacs with a Doom configuration(https://github.com/hlissner/doom-emacs)
+ Duck Duck Go search engine on this browser
+ Hexchat for my IRC Client
+ Neovim
+ ZSH with Oh My ZSH with a few plugins here and there(but let's just just not get into that)
+ vifm for File Manager. Still have nnn and Nautilus installed


