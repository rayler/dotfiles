;;; config.el -*- lexical-binding: t; -*-
;;; My stuff
;;;; Code:
(setq display-line-numbers-type 'visual) ;; Set hybrid lines

(setq user-full-name "Rishabh Tewari"
      user-mail-address "rishabhtewari1@gmail.com")

(global-visual-line-mode 1) ;; Line Wrapping 1 for on, 0 for off.

;; (setq
 ;; doom-font (font-spec :family "Hack" :size 16)
;; )
;; (setq doom-font (font-spec :size 16))

(require 'doom-peacock-theme)

;; (require 'doom-nova-padded-modeline)

(map! :m "M-j" #'multi-next-line
      :m "M-k" #'multi-previous-line

      ;; Easier window movement
      :n "C-h" #'evil-window-left
      :n "C-j" #'evil-window-down
      :n "C-k" #'evil-window-up
      :n "C-l" #'evil-window-right

      ;; Resizing Windows
      :n "C-S-H" #'evil-window-decrease-width
      :n "C-S-L" #'evil-window-increase-width
      :n "C-S-K" #'evil-window-increase-height
      :n "C-S-J" #'evil-window-decrease-height

      :n "C-Q" #'evil-quit ;;Closing Window
      :n "C-q" #'kill-buffer ;;Closing Buffer

      (:map vterm-mode-map
        ;; Easier window movement
        :i "C-h" #'evil-window-left
        :i "C-j" #'evil-window-down
        :i "C-k" #'evil-window-up
        :i "C-l" #'evil-window-right
        ;; Resizing Windows
        :i "C-S-h" #'evil-window-decrease-width
        :i "C-S-l" #'evil-window-increase-width
        :i "C-S-k" #'evil-window-increase-height
        :i "C-S-j" #'evil-window-decrease-height
        )

      (:map evil-treemacs-state-map
        "C-h" #'evil-window-left
        "C-l" #'evil-window-right
        "M-j" #'multi-next-line
        "M-k" #'multi-previous-line))
;; company-mode auto-completion, to turn off use :company-complete
(require 'company)
(setq company-idle-delay 0.1
      company-minimum-prefix-length 3)

(add-hook 'prog-mode-hook #'goto-address-mode) ;; Linkify links!

;; Stuff for Haskell
(add-hook 'haskell-mode-hook 'turn-on-haskell-doc-mode)

;; hslint on the command line only likes this indentation mode;
;; alternatives commented out below.
(add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)
;;(add-hook 'haskell-mode-hook 'turn-on-haskell-indent)
;;(add-hook 'haskell-mode-hook 'turn-on-haskell-simple-indent)

;; Ignore compiled Haskell files in filename completions
(add-to-list 'completion-ignored-extensions ".hi")
;; do linting on-the-fly
(with-eval-after-load 'intero
  (flycheck-add-next-checker 'intero '(warning . haskell-hlint))
  )

(map! :after pdf-tools
      :map pdf-view-mode-map
      "i" (λ! (pdf-view-midnight-minor-mode)))


(provide 'config)

(after! pdf-view-mode
  (map! :leader
    :prefix "f"
    "f" #'pdf-links-action-perform
    "i" #' pdf-view-midnight-minor-mode))

;; Modules
;;
(load! "+org") ;; Org Mode For Flours
;; (load! "+irony")
