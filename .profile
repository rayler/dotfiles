# bindkey -v #vi mode in ZSH
set -o vi #vi mode in BASH
export EDITOR="nvim"
export TERMINAL="st"
export BROWSER="firefox-esr"
export READER="zathura"
export FILE="ranger"
# PATHS Here
export PATH=$PATH:/sbin
export PATH="$HOME/.cargo/bin:$PATH"
export PATH="$HOME/.emacs.d/bin:$PATH"
export PATH="$HOME/.cabal/bin/:$PATH"
export PATH="$PATH:$(du "$HOME/.local/bin/" | cut -f2 | tr '\n' ':' | sed 's/:*$//')"
if [ -d "$HOME/.scripts/:$PATH" ]
then
	export PATH="$PATH:$(du "$HOME/.scripts/" | cut -f2 | tr '\n' ':' | sed 's/:*$//')"
fi

export PATH="$HOME/.repos/:$PATH"
if [ -d "$HOME/.repos/macbook-lighter/src" ]
then
	export PATH="$HOME/repos/macbook-lighter/src/:$PATH"
fi

# less/man colors
export LESS=-R
export LESS_TERMCAP_mb="$(printf '%b' '[1;31m')"; a="${a%_}"
export LESS_TERMCAP_md="$(printf '%b' '[1;36m')"; a="${a%_}"
export LESS_TERMCAP_me="$(printf '%b' '[0m')"; a="${a%_}"
export LESS_TERMCAP_so="$(printf '%b' '[01;44;33m')"; a="${a%_}"
export LESS_TERMCAP_se="$(printf '%b' '[0m')"; a="${a%_}"
export LESS_TERMCAP_us="$(printf '%b' '[1;32m')"; a="${a%_}"
export LESS_TERMCAP_ue="$(printf '%b' '[0m')"; a="${a%_}"

if [ -d "$HOME/.repos/macbook-lighter/src" ]
then
	alias q="sudo $HOME/.repos/macbook-lighter/src/macbook-lighter-screen.sh -i 60" # My keyboard backlight doesn't work anyways, so only screen will suffice
	alias a="sudo $HOME/.repos/macbook-lighter/src/macbook-lighter-screen.sh -d 60" # My keyboard backlight doesn't work anyways, so only screen will suffice
fi

[ -f "$HOME/.config/shortcutrc" ] && source "$HOME/.config/shortcutrc" # Load shortcut aliases
[ -f "$HOME/.config/aliasrc" ] && source "$HOME/.config/aliasrc"

## Works for ZSH Only not BASH

## Filetype openers
#alias -s pdf=$READER
#alias -s md=$EDITOR

##global aliases
#alias -g A="| ag"
#alias -g G="| grep"
#alias -g L="| less" # Who uses more when you can use less


# Switch escape and caps if tty:
sudo -n loadkeys ~/.local/bin/ttymaps.kmap 2>/dev/null

TZ='America/Chicago'; export TZ
